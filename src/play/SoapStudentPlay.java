package play;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.javacodegeeks.enterprise.ws.GetStudentRequest;
import com.javacodegeeks.enterprise.ws.GetStudentResponse;

/**
 * Created by oster on 2016/4/12.
 */
public class SoapStudentPlay {

    public static void main(String args[]) throws Exception {
        // Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        String url = "http://localhost:8080/HelloWebService/service/myWebService";
        SOAPMessage soapResponse = soapConnection.call(createStudentRequest(), url);

        // print SOAP Response
//        System.out.println("Response SOAP Message:");
//        soapResponse.writeTo(System.out);

        System.out.println();
        System.out.println("Transfer to Object - Call transfer()");
        if (soapResponse.getSOAPBody().hasFault()) {
        	soapResponse.writeTo(System.out);
        	return;
        }
        
//        GetStudentResponse responseObject = transferResponse(GetStudentResponse.class, soapResponse);
        GetStudentResponse responseObject = transferToResult(GetStudentResponse.class, "getStudentResponse", soapResponse);
        System.out.println();
        System.out.println("Final Object:" + responseObject);
        System.out.println(responseObject.getClass().getSimpleName() + " age: " + responseObject.getStudent().getAge());

        soapConnection.close();
    }

    private static SOAPMessage createStudentRequest() throws Exception {

        //Initial Soap Body Object
        GetStudentRequest getStudentRequest = new GetStudentRequest(); 
        getStudentRequest.setStudentId(23);

        //
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://ws.enterprise.javacodegeeks.com/MyWebService/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
//        envelope.addNamespaceDeclaration("std", serverURI);

        // marshall Body Object to W3c document, document to SOAP Body
        Document bodyDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        Marshaller marshaller = JAXBContext.newInstance(GetStudentRequest.class).createMarshaller();
        marshaller.marshal(getStudentRequest, bodyDocument);

        envelope.getBody().addDocument(bodyDocument);

        // Set Protocal Header
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI + "getStudent");
        soapMessage.saveChanges();

        /* Print the request message */
        System.out.print("Create Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }

    private static <T> T transferResponse(Class<T> targetClass, SOAPMessage soapResponse) throws SOAPException, IOException {
        soapResponse.writeTo(System.out); 
        SOAPBody body = soapResponse.getSOAPBody();
        //bodyElement.

        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);

        System.out.println("Body.firstElement = " + body.getFirstChild().toString());
        return xmlMapper.readValue(body.getFirstChild().toString(), targetClass);
    }
    
    private static <T> T transferToResult(Class<T> targetClass, String tagName, SOAPMessage soapResponse) throws JAXBException, XMLStreamException, IOException, SOAPException {
        soapResponse.writeTo(System.out);
//        XmlRootElement a = targetClass.getAnnotation(XmlRootElement.class);
//        a.name(); By Mei Mei Solution...
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        soapResponse.writeTo(bos);
        XMLInputFactory xif = XMLInputFactory.newFactory();
        XMLStreamReader xsr = xif.createXMLStreamReader(new StreamSource(new ByteArrayInputStream(bos.toByteArray())));

        xsr.nextTag();
        while (!xsr.getLocalName().equals(tagName)) {
            xsr.nextTag();
        }

        JAXBContext jc = JAXBContext.newInstance(targetClass);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        JAXBElement<T> jb = unmarshaller.unmarshal(xsr, targetClass);
        xsr.close();

        return jb.getValue();

    }


}
